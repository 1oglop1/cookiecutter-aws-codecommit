AWS CodeCommit git repo
=======================

What is this?
=============

This is a cookiecutter workaround for dealing with AWS CodeCommit repositories and aws git credential helper. 

How to use this
===============

Prerequisites: `pip install cookiecutter` - you need to have cookiecutter installed https://cookiecutter.readthedocs.io/en/latest/installation.html


#. Create repository in AWS CodeCommit
#. Copy clone url: eg. ``https://git-codecommit.eu-west-1.amazonaws.com/v1/repos/<reponame>``
#. Download and use the AWS codecommit cookiecutter ``cookiecutter https://gitlab.com/1oglop1/cookiecutter-aws-codecommit.git``
#. Cookiecutter asks you to insert CodeCommit Clone url ``https://git-codecommit.eu-west-1.amazonaws.com/v1/repos/<reponame>``\
#. Type directory name or leave default ``<reponame>>``

Now the cookiecutter is cached in your computer ``$HOME/.cookiecutters`` and you can use it like: ``cookiecutter cookiecutter-aws-codecommit``.
(In case you want to update to new version just use full url again)
I recommend to set alias ``alias aws-cc-clone="cookiecutter cookiecutter-aws-codecommit"``.