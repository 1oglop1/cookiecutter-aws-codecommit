import os
from subprocess import call
from shutil import move

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)
repo_url = "{{ cookiecutter.git_repo_url }}"
if __name__ == '__main__':
    print('Creating new repository in', PROJECT_DIRECTORY)
    call(["git", "init"])
    os.rename("config", ".git/config")
    call(["git", "pull", "origin", "master"])

